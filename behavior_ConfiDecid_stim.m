
clearvars
close all

% Analyses
% 1 : Effect of stimulation and anatomical subregion on the variable of interest
% 2 : Effect of stimulation on kG and kL in the choice model
% 3 : Same as analysis 1, but creates clusters with k-means on a region
% 4 : Behavioural analyses on all electrodes in the table for NON stimulated trials

%% Options

opt.parcels = {'aINS'}; % 'PFCvm', 'aINS', 'vaIns', 'daIns', 'SU-ROS', 'ROS-S', 'LD8-9', 'LD6-7'
opt.var_of_int = 'p(accept)'; % 'p(accept)' | 'Confidence'

opt.analyse = 4;

% What needs to be defined :
% Analyse 1 : 2 parcels + var_of_int
% Analyse 2 : 2 parcels
% Analyse 3 : 1 parcel + var_of_int
% Analyse 4 : var_of_int

%% Paramètres

init.bdd_path = ''; % Data table path (if empty = current folder)
init.pathtofig = ''; % Path where generated figures will be saved

% Load the table with all infos
% load(fullfile(init.bdd_path, 'roi_data.mat'), 'roi_data')
load('roi_data.mat')

% Define column according to parcel name
if all(ismember(opt.parcels, {'PFCvm', 'aINS'}))
    opt.roi_col = 'roi_name';
elseif all(ismember(opt.parcels, {'daIns', 'vaIns', 'SU-ROS', 'ROS-S'}))
    opt.roi_col = 'sub_roi';
elseif all(ismember(opt.parcels, {'LD6-7', 'LD8-9'}))
    opt.roi_col = 'elec_julich_conc';
end

% Usefull columns
init.var_col.gain = 4;
init.var_col.loss = 5;
init.var_col.diff = 6;
init.var_col.choice = 7;
init.var_col.choice_rt = 8;
init.var_col.confidence = 9;

% Define column according to the variable of interest
if ismember(opt.var_of_int, {'p(accept)'})
    
    opt.var_column = init.var_col.choice; % Choix (1 = accept ; 0 = reject)
    opt.ylabel = 'p(accept)';
    opt.ylim = [0 1];
    opt.YTick = (0:.2:1);
    
elseif ismember(opt.var_of_int, {'Confidence'})
    
    opt.var_column = init.var_col.confidence; % Confidence (already z-scored by SESSIONS)
    opt.ylabel = 'Confidence';
    opt.ylim = [-0.5 0.5];
    opt.YTick = (-0.5:0.5:0.5);
    
end

% Create folder
init.pathtofig_parcl = fullfile(init.pathtofig, strjoin(sort(opt.parcels), '-'));

if ~exist(fullfile(init.pathtofig_parcl, opt.ylabel), 'dir')
    mkdir(fullfile(init.pathtofig_parcl, opt.ylabel));
end

%% Analyses

switch opt.analyse
    
    case 1
        % ANALYSE 1 : Effect of stimulation and anatomical subregion on the variable of interest
        test_stim_roi(roi_data, opt, init)
        
    case 2
        % ANALYSE 2 : Effect of stimulation on kG and kL in the choice model
        if length(opt.parcels) == 1
            [parcl_tbl, opt] = test_stim_roi_mni(roi_data, opt, init); % Run ANALYSE 3 to get regions ID
            stim_effect_on_choice_model(parcl_tbl, opt, init)
        else
            stim_effect_on_choice_model(roi_data, opt, init)
        end
        
    case 3
        % ANALYSE 3 : Effect of stimulation and statistically defined clusters on the variable of interest
        test_stim_roi_mni(roi_data, opt, init)
        
    case 4
        % ANALYSE 4 : Behavioural analyses on all electrodes in the table for NON stimulated trials
        behavior(roi_data, opt, init)
        
end

%% FUNCTIONS

function test_stim_roi(roi_data, opt, init)

idx = 1;

for parcl = 1:length(opt.parcels) % Boucle à travers les parcelles
    
    clearvars -except roi_data init opt parcl fig_data glme_data idx
    parcl_tbl = roi_data(ismember(roi_data.(opt.roi_col), opt.parcels{parcl}),:);
    
    for elec = 1:height(parcl_tbl)
        
        elec_data = parcl_tbl.elec_data{elec};
        z_var = elec_data(:,opt.var_column); % Var of interest already z-scored by subject (only if not binomial)
        
        no_stim(elec,:) = mean(z_var(elec_data(:,2) == 2)); % No stim trials
        stim(elec,:) = mean(z_var(elec_data(:,2) == 200)); % Stim trials
        
        glme_data.responses(idx:idx+1,:) = [no_stim(elec,:); stim(elec,:)];
        glme_data.stim_condi(idx:idx+1,:) = {'no-stim'; 'stim'};
        glme_data.roi(idx:idx+1,:) = repmat(opt.parcels(parcl), 2, 1);
        glme_data.subj_id(idx:idx+1,:) = repmat({sprintf('s%d', parcl_tbl.subj_id(elec))}, 2, 1);
        
        elec_id = {sprintf('%d_%s', parcl_tbl.subj_id(elec), parcl_tbl.elec_name{elec})};
        glme_data.elec_id(idx:idx+1,:) = repmat(elec_id, 2, 1);

        if ~isnan(glme_data.responses(idx))
            fig_data.subj{parcl,:}(elec,:) = {sprintf('s%d', parcl_tbl.subj_id(elec))};
        end

        idx = idx + 2;
        
    end % End of the loop over electrodes
    
    % Remove NaN
    nan_idx = isnan(no_stim) | isnan(stim);
    no_stim(nan_idx,:) = [];
    stim(nan_idx,:) = [];
    
    fig_data.responses(parcl,:) = {no_stim, stim}; % Lines = ROI / Columns = Stim condition
    fig_data.subj{parcl,:}(cellfun(@isempty, fig_data.subj{parcl,:})) = [];
    
end % End of the loop over parcels

%% Mixed-effects model

% Est-ce que la stim et/ou la ROI prédit la variable d'intérêt (e.g. p(accept)) ?

glme_tbl = struct2table(glme_data);

if ismember(opt.var_of_int, {'Confidence'})
    glme_model = 'responses ~ stim_condi * roi + (1 + stim_condi | elec_id) + (-1 + stim_condi + roi| subj_id) + (1 + stim_condi:roi| subj_id)';
else
    glme_model = 'responses ~ stim_condi * roi + (1 + stim_condi | elec_id) + (1 + stim_condi*roi| subj_id)';
    % glme_model = 'responses ~ stim_condi + (1 + stim_condi | elec_id) + (1 + stim_condi| subj_id)'; % Test the effect of stimulation on one ROI alone
end

[glme, post_hoc] = glme_and_post_hoc(glme_tbl, glme_model, opt.parcels);
disp(anova(glme))
disp(post_hoc.test)

%% Figure

fig = figure;
hold on

for r = 1:length(opt.parcels) % Loop through subregions

    mean_r = mean(fig_data.responses{r,2} - fig_data.responses{r,1});
    sem_r = std(fig_data.responses{r,2} - fig_data.responses{r,1})/sqrt(length(fig_data.responses{r,1}));
    errorbar(r, mean_r, sem_r, '.-', 'MarkerSize', 15)

    diff{r} = fig_data.responses{r,2} - fig_data.responses{r,1};

    jit = round([r-0.1 r+0.1] * 30);
    jitX{r} = randi(jit, [1 numel(diff{r})]) / 30;
    while length(unique(round([jitX{r}' diff{r}],2), 'rows')) ~= length(diff{r})
        jitX{r} = randi(jit, [1 numel(diff{r})]) / 30;
    end

    scatter(jitX{r}, diff{r});

end % End of the loop through subregions

% Loop through the electrodes of one region to find electrodes from same subjects
reg_subj = fig_data.subj{1};
other_reg = fig_data.subj{2};

for elec = 1:numel(reg_subj)

    elec_subj = reg_subj(elec);
    same_subj = find(ismember(other_reg, elec_subj));

    for same = 1:numel(same_subj)

        lineY = [diff{1}(elec) diff{2}(same_subj(same))];
        lineX = [jitX{1}(elec) jitX{2}(same_subj(same))];
        plot(lineX, lineY, 'k-')

    end

end % End of the loop through stimulation sites

xticks(1:length(opt.parcels))
xticklabels(opt.parcels)
ylabel(sprintf('%s_{stim - no stim}', opt.var_of_int))

xlim([0.6 2.6])
if ismember(opt.var_of_int, {'p(accept)'})
    ylim([-0.4 0.5])
elseif ismember(opt.var_of_int, {'Confidence'})
    ylim([-1.2 0.8])
end

% Save
fig_name = fullfile(init.pathtofig_parcl, opt.ylabel, sprintf('%s_stim_and_anat_subregions_effect_on_%s.tiff', strjoin(opt.parcels, '_'), opt.var_of_int));
fig.PaperPosition = [0, 0, 17 20]; % [left bottom width height]
print(fig, fig_name, '-dtiff', '-r200');

close(fig)

end

function stim_effect_on_choice_model(roi_data, opt, init)

stats.idx = 1;

if length(opt.parcels) == 1
    opt.parcels = opt.region;
    opt.roi_col = 'region_id';
end

for parcl = 1:length(opt.parcels) % Boucle à travers les parcelles
    
    clearvars -except roi_data init opt parcl fig stats
    parcl_tbl = roi_data(ismember(roi_data.(opt.roi_col), opt.parcels{parcl}),:);
    
    for elec = 1:height(parcl_tbl) % Loop over electrodes in the ROI
        
        elec_choice_model = parcl_tbl.choice_model(elec);
        
        param_id = {'kG', 'kL'};
        
        for param = 1:length(param_id) % Loop over kG and kL param
            
            param_idx = elec_choice_model.options.inF.thetaLabel.(param_id{param});
            no_stim.(param_id{param})(elec,:) = elec_choice_model.no_stim.posterior.muTheta(param_idx); % No stim trials
            stim.(param_id{param})(elec,:) = elec_choice_model.stim.posterior.muTheta(param_idx); % Stim trials
            elec_id = {sprintf('%d_%s', parcl_tbl.subj_id(elec), parcl_tbl.elec_name{elec})};
            subj_id = {sprintf('s%d', parcl_tbl.subj_id(elec))};

            % Data for GLME
            stats.glme_data.responses(stats.idx:stats.idx+1,:) = [no_stim.(param_id{param})(elec,:); stim.(param_id{param})(elec,:)];
            stats.glme_data.stim_condi(stats.idx:stats.idx+1,:) = {'no_stim'; 'stim'}; % -1 = no-stim = reference group ; 1 = stim (effects coding)
            stats.glme_data.param(stats.idx:stats.idx+1,:) = repmat(param_id(param), 2, 1);
            stats.glme_data.roi(stats.idx:stats.idx+1,:) = repmat(opt.parcels(parcl), 2, 1);
            stats.glme_data.elec_id(stats.idx:stats.idx+1,:) = repmat(elec_id, 2, 1);
            stats.glme_data.subj_id(stats.idx:stats.idx+1,:) = repmat(subj_id, 2, 1);
            stats.idx = stats.idx + 2;
            
        end % End of the loop over param
    end % End of the loop over electrodes
end % End of the loop over parcels

%% Mixed-effects model

glme_tbl = struct2table(stats.glme_data);

if ismember(opt.var_of_int, {'Confidence'})
    glme_model = 'responses ~ stim_condi * roi + (1 + stim_condi | elec_id) + (-1 + stim_condi + roi| subj_id) + (1 + stim_condi:roi| subj_id)';
else
    glme_model = 'responses ~ stim_condi * roi + (1 + stim_condi | elec_id) + (1 + stim_condi*roi| subj_id)';
end

for p = 1:length(param_id)
    glme_tbl_p = glme_tbl(ismember(glme_tbl.param, param_id{p}),:);
    [glme{p}, post_hoc{p}] = glme_and_post_hoc(glme_tbl_p, glme_model, opt.parcels);

    fprintf('%s:', param_id{p})
    disp(anova(glme{p}))
    celldisp(post_hoc{p}.test, 'Post-hoc')
    
    stats.glme_post_hoc_txt{p,1} = sprintf('%s - %s : p = %.3g', opt.parcels{1}, param_id{p}, post_hoc{p}.pVal(1));
    stats.glme_post_hoc_txt{p,2} = sprintf('%s - %s : p = %.3g', opt.parcels{2}, param_id{p}, post_hoc{p}.pVal(2));
end

end

function [parcl_tbl, opt] = test_stim_roi_mni(roi_data, opt, init)

idx = 1;
parcl_tbl = roi_data(ismember(roi_data.(opt.roi_col), opt.parcels),:);

% Clusters done with p(accept) only

% Create data structure
mod.X = cellfun(@(x) x(:,1), parcl_tbl.elec_MNI);
mod.Y = cellfun(@(x) x(:,2), parcl_tbl.elec_MNI);
mod.Z = cellfun(@(x) x(:,3), parcl_tbl.elec_MNI);

no_stim = cellfun(@(x) mean(x(x(:,2) == 2,init.var_col.choice)), parcl_tbl.elec_data);
stim = cellfun(@(x) mean(x(x(:,2) == 200,init.var_col.choice)), parcl_tbl.elec_data);
mod.voi = stim - no_stim;

num_data = zscore([abs(mod.X), mod.Y, mod.Z]);

mod.num_voi = mod.voi;
mod.voi(mod.voi < 0) = -1;
mod.voi(mod.voi > 0) = 1;

cat_data = (mod.voi - nanmean(mod.voi)) / nanstd(mod.voi);

data = [num_data, cat_data];

% K-means
nb_clust = 2;
[k_idx, ~] = kmeans(data, nb_clust, 'Replicates', 100);
u_kidx = unique(k_idx(~isnan(k_idx)));

for i = 1:length(u_kidx)
    
    opt.region(i) = {sprintf('clust%d', u_kidx(i))};
    parcl_tbl.region_id(k_idx == u_kidx(i)) = opt.region(i);
    
end

% Deal with NaN
parcl_tbl.region_id(cellfun(@isempty, parcl_tbl.region_id)) = {''};

% Plot p(accept) + boundaries
figure
scatter(mod.Y, mod.Z, [], mod.voi, 'filled')
colormap jet(3)
colormap([248, 51, 60 ; 252, 171, 16 ; 68, 175, 105]/255)
caxis([-0.001 0.001])
colorbar('Ticks',[-0.6e-3 0 0.6e-3],'TickLabels',{'Negative','Zero','Positive'})
title(sprintf('%s - p(accept)_{stim - no stim}', opt.parcels{:}))
xlabel('Y')
ylabel('Z')

% Direction
% Regression of the variable of interest against MNI coordinates
mod.subj = parcl_tbl.subj_id;
mod_tbl = struct2table(mod);
mdl = fitglme(mod_tbl, 'voi ~ X + Y + Z + (X + Y + Z | subj)');

Xlim = xlim;
Ylim = ylim;

hold on

switch opt.parcels{:}
    case 'PFCvm'
        [Y,Z] = meshgrid(linspace(Xlim(1), Xlim(2), 8), linspace(Ylim(1), Ylim(2), 5));
    case 'aINS'
        [Y,Z] = meshgrid(linspace(Xlim(1), Xlim(2), 7), linspace(Ylim(1), Ylim(2), 6));
end

U = repmat(mdl.Coefficients.Estimate(ismember(mdl.CoefficientNames, 'Y')), size(Y,1), size(Y,2));
V = repmat(mdl.Coefficients.Estimate(ismember(mdl.CoefficientNames, 'Z')), size(Z,1), size(Z,2));

quiver(Y,Z,U,V)

xlim(Xlim)
ylim(Ylim)

% Plot contour
hold on;
for i = 1:max(k_idx)
    P = [mod.Y(k_idx == i), mod.Z(k_idx == i)];
    k = convhull(P);
    % plot(P(k,1),P(k,2))
    patch(P(k,1),P(k,2),[i-1 0 i/2], 'FaceAlpha', .3)
end

close all

%-------------------------------------------------------------------------%

for reg = 1:length(opt.region) % Boucle à travers les regions
    
    clearvars -except parcl_tbl init opt reg anova_data glme_data idx
    reg_tbl = parcl_tbl(ismember(parcl_tbl.region_id, opt.region{reg}),:);
    
    for elec = 1:height(reg_tbl)
        
        elec_data = reg_tbl.elec_data{elec};
        z_var = elec_data(:,opt.var_column); % Var of interest already z-scored by subject (only if not binomial)
        
        no_stim(elec,:) = mean(z_var(elec_data(:,2) == 2)); % No stim trials
        stim(elec,:) = mean(z_var(elec_data(:,2) == 200)); % Stim trials
        elec_id = {sprintf('%d_%s', reg_tbl.subj_id(elec), reg_tbl.elec_name{elec})};
        subj_id = {sprintf('s%d', reg_tbl.subj_id(elec))};
        
        glme_data.responses(idx:idx+1,:) = [no_stim(elec,:); stim(elec,:)];
        glme_data.stim_condi(idx:idx+1,:) = {'no-stim'; 'stim'};
        glme_data.roi(idx:idx+1,:) = repmat(opt.region(reg), 2, 1);
        glme_data.elec_id(idx:idx+1,:) = repmat(elec_id, 2, 1);
        glme_data.subj_id(idx:idx+1,:) = repmat(subj_id, 2, 1);
        idx = idx + 2;
        
    end % End of the loop over electrodes
    
    % Remove NaN
    nan_idx = isnan(no_stim) | isnan(stim);
    no_stim(nan_idx,:) = [];
    stim(nan_idx,:) = [];
    
    anova_data.responses(reg,:) = {no_stim, stim}; % Lines = ROI / Columns = Stim condition
    anova_data.stim_condi(reg,:) = {repmat({'no-stim'}, length(stim), 1), repmat({'stim'}, length(no_stim), 1)};
    anova_data.roi(reg,:) = {repmat(opt.region(reg), length(stim), 1), repmat(opt.region(reg), length(no_stim), 1)};
    
end % End of the loop over region

%% Mixed-effects model

glme_tbl = struct2table(glme_data);
if ismember(opt.var_of_int, {'Confidence'})
    glme_model = 'responses ~ stim_condi * roi + (1 + stim_condi | elec_id) + (-1 + stim_condi + roi| subj_id) + (1 + stim_condi:roi| subj_id)';
else
    glme_model = 'responses ~ stim_condi * roi + (1 + stim_condi | elec_id) + (1 + stim_condi*roi| subj_id)';
end

[glme, post_hoc] = glme_and_post_hoc(glme_tbl, glme_model, opt.region);

clc
disp(anova(glme))
celldisp(post_hoc.test, 'Post-hoc')

end

function behavior(roi_data, opt, init)

%% Variable of interest according to gains/loss/diff
% Var of interest is the response

prdct = {'gain', 'loss', 'diff'}; % 'gain', 'loss', 'diff' | 'confidence'
resp = regexprep(opt.var_of_int, {'[%() ]+', '_+$'}, {'_', ''}); % Remove forbidden char

for elec = 1:height(roi_data) % Loop over electrodes
    
    no_stim_data = roi_data.elec_data{elec}(roi_data.elec_data{elec}(:,2) == 2,:); % No stim
    var = no_stim_data(:,opt.var_column);
    
    for dim = 1:length(prdct) % Loop over predictors
        
        prospect = no_stim_data(:,init.var_col.(prdct{dim})); % No stim trials
        
        switch prdct{dim}
            case {'gain', 'loss'}
                id_q1 = prospect < 20;
                id_q2 = prospect < 30 & prospect >= 20;
                id_q3 = prospect < 40 & prospect >= 30;
                id_q4 = prospect < 50 & prospect >= 40;
            case {'diff', 'confidence'}
                id_q1 = prospect < prctile(prospect,25);
                id_q2 = prospect < prctile(prospect,50) & prospect >= prctile(prospect,25);
                id_q3 = prospect < prctile(prospect,75) & prospect >= prctile(prospect,50);
                id_q4 = prospect >= prctile(prospect,75);
        end
        
        fig_data(elec,:,dim) = [mean(var(id_q1)), mean(var(id_q2)), mean(var(id_q3)), mean(var(id_q4))]; % elec by task_levels (4 levels) by task dim (gain, loss)
        
        % Retrieve data for stats
        glme_data.(prdct{dim})(elec,:) = {prospect};
        
    end % End of the loop over dimensions

    elec_id = {sprintf('%d_%s', roi_data.subj_id(elec), roi_data.elec_name{elec})};
    subj_id = {sprintf('s%d', roi_data.subj_id(elec))};
    
    glme_data.(resp)(elec,:) = {var};
    glme_data.subj{elec,:} = repmat(subj_id, length(var), 1);
    glme_data.sess{elec,:} = repmat(elec_id, length(var), 1);
    
end % End of the loop over electrodes

% STATS ------------------------------------------------------------------%

glme_data.(resp) = cat(1, glme_data.(resp){:});
for dim = 1:length(prdct)
    glme_data.(prdct{dim}) = cat(1, glme_data.(prdct{dim}){:});
end
glme_data.subj = cat(1, glme_data.subj{:});
glme_data.sess = cat(1, glme_data.sess{:});

glme_tbl = struct2table(glme_data);

if VBA_isBinary(glme_tbl.(resp))
    distrib = 'Binomial';
else
    distrib = 'Normal';
end

glme_model = sprintf('%1$s ~ %2$s + (1 + %2$s | sess) + (1 + %2$s | subj)', resp, strjoin(prdct, ' + '));
glme = fitglme(glme_tbl, glme_model, 'Distribution', distrib);

clc
disp(glme.Coefficients)

% FIGURE -----------------------------------------------------------------%

fig = figure;
hold on;

for dim = 1:size(fig_data,3) % task_dim : 1 = gain, 2 = loss
    
    % FIGURE : proba d'accepter pour chaque dimension (gain, loss) toutes les électrodes
    sub = subplot(1,size(fig_data,3),dim);
    hold on;
    
    errorbar(nanmean(fig_data(:,:,dim)), nanstd(fig_data(:,:,dim))/sqrt(size(fig_data,1)), 'o-', 'MarkerSize', 3);
    
    ylabel(opt.ylabel)
    ax = gca;
    ax.XTick = [1 2 3 4];
    ax.YTick = opt.YTick;
    xlim([0.5 4.5])
    ylim(opt.ylim)
    
    switch prdct{dim}
        case {'gain', 'loss'}
            set(gca,'XTickLabel',{'[1-2]',']2-3]',']3-4]',']4-5]'})
            xlabel(sprintf('%s prospect (€)', [upper(extractBefore(prdct{dim},2)) extractAfter(prdct{dim},1)]))
        case {'diff', 'confidence'}
            set(gca,'XTickLabel',{'[0-25]',']25-50]',']50-75]',']75-100]'})
            xlabel(sprintf('%s (percentile)', [upper(extractBefore(prdct{dim},2)) extractAfter(prdct{dim},1)]))
    end
    
    % GLME result
    annotation('textbox', 'String', sprintf('Mixed-effects model\n%s : p = %.3g',...
        prdct{dim}, glme.Coefficients.pValue(strcmp(glme.CoefficientNames, prdct{dim}))),...
        'Position', sub.Position, 'Vert', 'top', 'Hori', 'center', 'FitBoxToText', 'on', 'FontSize', 8, 'Interpreter', 'none')
    
end

sgtitle(glme_model)
fig.PaperPosition = [0, 0, 8*dim 8];

end

function [glme, post_hoc] = glme_and_post_hoc(glme_tbl, glme_model, roi_names)

% Compute glme and post hoc for interactions
%
% INPUT :
%   - glme_tbl = table for fitglme
%   - glme_model = formula for fitglme

% See http://singmann.org/download/publications/singmann_kellen-introduction-mixed-models.pdf
% for why it's better to use effect coding than dummy coding when there is an interaction

% Since our design is unbalanced (different number of data per parcel),
% we use "weighted effect coding" for the ROIs
% (https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5288425/pdf/38_2016_Article_901.pdf)

% See also this reference for a discussion of significance testing for LMEs:
% https://doi.org/10.3758/s13428-016-0809-y

glme = fitglme(glme_tbl, glme_model, 'DummyVarCoding', 'effects');

roi_1 = regexp(glme.CoefficientNames, '(?<=^(roi_)).*', 'match'); % Find roi_1
roi(1) = roi_1{~cellfun(@isempty, roi_1)};
roi(2) = roi_names(~ismember(roi_names, roi(1)));

% Post-hoc testing using coefTest ----------------------------------------%

% We need to specify a contrast matrix H
% Columns of H represent the number of fixed-effects coefficients in glme = length(glme.CoefficientNames)
% Each row of H represents one contrast
% See : https://fr.mathworks.com/matlabcentral/answers/1701785-pairwise-post-hoc-testing-using-coeftest

% Here :
% H = [0,0,0,1] = Stim condi vs. ROI interaction (can be checked with anova(glme))

% CONDITIONS :
% SC = no-stim, stim
% roi = 1, 2 (e.g. daIns, vaIns)
% SC * roi

% SC_no-stim:roi_1 + SC_stim:roi_1 = 0
% SC_no-stim:roi_2 + SC_stim:roi_2 = 0
%                 or
% SC_no-stim:roi_1 + SC_no-stim:roi_2 = 0;
% SC_stim:roi_1 + SC_stim:roi_2 = 0

% So :
% SC_no-stim:roi_1 - SC_stim:roi_1 =
% 2 * SC_no-stim:roi_1

% But 'SC_no-stim:roi_1 - SC_no-stim:roi_2' get the same result :
% SC_no-stim:roi_1 - SC_no-stim:roi_2 = 2 * SC_no-stim:roi_1

% So we need to do this :

% ROI 1 no stim VS. ROI 1 stim :
% SC_no-stim + ROI_1 + SC_no-stim:roi_1 - SC_stim - ROI_1 - SC_stim:roi_1 =
% SC_no-stim - SC_stim + SC_no-stim:roi_1 - SC_stim:roi_1 =
% 2 * SC_no-stim + 2 * SC_no-stim:roi_1

% ROI 2 no stim VS. ROI 2 stim :
% SC_no-stim + ROI_2 + SC_no-stim:roi_2 - SC_stim - ROI_2 - SC_stim:roi_2 =
% SC_no-stim - SC_stim + SC_no-stim:roi_2 - SC_stim:roi_2 =
% 2 * SC_no-stim - SC_no-stim_roi_1 + SC_stim_roi_1 =
% 2 * SC_no-stim - 2 SC_no-stim_roi_1

%        [Int, SC_no-stim, ROI_1, SC_no-stim:ROI_1]
H(1,:) = [ 0,       2,       0,           2       ]; % ROI 1 no stim VS. ROI 1 stim
H(2,:) = [ 0,       2,       0,          -2       ]; % ROI 2 no stim VS. ROI 2 stim

for i = 1:size(H,1)
    [post_hoc.pVal(i),F(i),DF1(i),DF2(i)] = coefTest(glme, H(i,:));
    post_hoc.test{i} = sprintf('%1$s_{no-stim} vs. %1$s_{stim} : F(%2$d,%3$d) = %4$.3g; p = %5$.3g', roi{i}, DF1(i), DF2(i), F(i), post_hoc.pVal(i));
end

end
