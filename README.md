# Direct stimulation of anterior insula and ventromedial prefrontal cortex disrupts economic choices

## Overview

This repository contains the Matlab code and data for:

**Direct stimulation of anterior insula and ventromedial prefrontal cortex disrupts economic choices**, *Romane Cecchi, Antoine Collomb-Clerc, Inès Rachidi, Lorella Minotti, Philippe Kahane, Mathias Pessiglione, Julien Bastin*, bioRxiv 2023.12.07.570630; doi: https://doi.org/10.1101/2023.12.07.570630

## System Requirements
### Hardware requirements

This code requires only a standard computer.

### Software requirements

- **Matlab** software (The MathWorks, Inc., USA) with the Matlab Statistical Toolbox.

The code was developed using **Matlab R2018b** and tested up to Matlab R2023b.

## Data

All the data needed to reproduce the results of the article with the `behavior_ConfiDecid_stim.m` code is in `roi_data.mat`.

## Instructions

1. Download the files `behavior_ConfiDecid_stim.m` and `roi_data.mat`.
2. Open `behavior_ConfiDecid_stim.m` in Matlab
3. Set `init.bdd_path` (line 26) to be the location where `roi_data.mat` is saved.
4. In order to run the desired test, it is necessary to edit the parameters on `lines 13 to 16` and then execute the code

## Examples

### Testing the effect of stimulation and anatomical subregions of the aIns on acceptance probability

```Matlab
opt.parcels = {'vaIns', 'daIns'};
opt.var_of_int = 'p(accept)'

opt.analyse = 1;
```

#### Output
- GLME and Post-hoc results
- Figure corresponding to **Fig. 2c**

### Testing the behavioural effect of task parameters (gain/loss/difficulty) on acceptance probability in trials without stimulation

- Lines `14` and `16`

```Matlab
opt.var_of_int = 'p(accept)';
opt.analyse = 4;
```
- Line `438`

```Matlab
prdct = {'gain', 'loss', 'diff'};
```

#### Output
- GLME results
- Figure corresponding to **Fig. 1d**